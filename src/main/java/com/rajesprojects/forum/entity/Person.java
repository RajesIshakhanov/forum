package com.rajesprojects.forum.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Entity
@Table(name="person")
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Username should not be empty.")
    @Size(min = 5, max = 50, message = "Username should be between 5 and 50 characters long.")
    @Column
    private String username;

    @NotEmpty(message = "Password should not be empty.")
    @Size(min = 5, max = 50, message = "Password should be between 5 and 50 characters long.")
    @Column
    private String password;

    @NotEmpty(message = "Email should not be empty.")
    @Column
    @Email
    private String email;
    @Column
    private String role;
    @Column
    private boolean enabled;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
    private List<Theme> themes;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
    private List<Comment> comments;


}
