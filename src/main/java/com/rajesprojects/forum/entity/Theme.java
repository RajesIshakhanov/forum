package com.rajesprojects.forum.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name="theme")
public class Theme {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Person person;

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private LocalDate createdDate;

    @Column
    private String status;

    @ToString.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "theme")
    private List<Comment> comments;
}
