package com.rajesprojects.forum.service;

import com.rajesprojects.forum.entity.Comment;
import com.rajesprojects.forum.entity.Person;
import com.rajesprojects.forum.entity.Theme;
import com.rajesprojects.forum.repository.CommentRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class CommentService implements CommentInterface {

    private final CommentRepository commentRepository;
    private final PersonDetailsService personDetailsService;

    public CommentService(CommentRepository commentRepository, PersonDetailsService personDetailsService) {
        this.commentRepository = commentRepository;
        this.personDetailsService = personDetailsService;
    }

    public List<Comment> getByThemeId(Long id) {
        return commentRepository.findAllByThemeId(id);
    }

    public Long getCommentsId() {
        Long currentId = commentRepository.getCurrentVal();
        return ++currentId;
    }

    public void addToComments(Theme theme, String commentDescription, String userName) {
        Long id = getCommentsId();
        Person person = personDetailsService.getPersonByName(userName);
        Comment comment = new Comment(id, theme, person, commentDescription, LocalDate.now());
        commentRepository.save(comment);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteCommentById(Long id) {
        commentRepository.deleteById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Transactional
    public void deleteAllCommentsByThemeId(Long id) {
        commentRepository.deleteAllByThemeId(id);
    }
}
