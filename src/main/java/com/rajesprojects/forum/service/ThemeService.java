package com.rajesprojects.forum.service;

import com.rajesprojects.forum.entity.Person;
import com.rajesprojects.forum.entity.Theme;
import com.rajesprojects.forum.repository.ThemeRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class ThemeService implements ThemeInterface {
    private final ThemeRepository themeRepository;
    private final PersonDetailsService personDetailsService;
    private final CommentService commentService;

    public ThemeService(ThemeRepository themeRepository, PersonDetailsService personDetailsService, CommentService commentService) {
        this.themeRepository = themeRepository;
        this.personDetailsService = personDetailsService;
        this.commentService = commentService;
    }

    public List<Theme> getThemes() {
        return themeRepository.findAll();
    }

    public Theme getThemeById(Long themeId) {
        return themeRepository.getById(themeId);
    }

    public Long getId() {
        Long currentId = themeRepository.getCurrentVal();
        return ++currentId;
    }

    public void addToThemes(Long personId, String themeName, String themeDescription) {
        Person person = personDetailsService.getPersonById(personId);
        Long id = getId();
        Theme theme = new Theme(id, person, themeName, themeDescription, LocalDate.now(), "open", new ArrayList<>());
        themeRepository.save(theme);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteThemeById(Long id) {
        commentService.deleteAllCommentsByThemeId(id);
        themeRepository.deleteById(id);
    }
}
