package com.rajesprojects.forum.service;

import com.rajesprojects.forum.entity.Person;

public interface RegistrationInterface {

    void register(Person person);
}
