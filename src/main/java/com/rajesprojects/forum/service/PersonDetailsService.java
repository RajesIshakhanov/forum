package com.rajesprojects.forum.service;

import com.rajesprojects.forum.entity.Person;
import com.rajesprojects.forum.repository.PeopleRepository;
import com.rajesprojects.forum.security.PersonDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class PersonDetailsService implements UserDetailsService {

    private final PeopleRepository peopleRepository;

    @Autowired
    public PersonDetailsService(PeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Person> person = peopleRepository.findByUsername(username);

        if (person.isEmpty()) {
            throw new UsernameNotFoundException("User not found!");
        }

        return new PersonDetails(person.get());
    }

    public Person getPersonById(Long personId) {
        return peopleRepository.getById(personId);
    }

    public Long getPersonIdByName(String username) {
        return peopleRepository.findPersonByUsername(username).getId();
    }

    public Person getPersonByName(String name) {
        return peopleRepository.findPersonByUsername(name);
    }
}
