package com.rajesprojects.forum.service;

import com.rajesprojects.forum.entity.Comment;
import com.rajesprojects.forum.entity.Theme;

import java.util.List;

public interface CommentInterface {

    List<Comment> getByThemeId(Long id);

    Long getCommentsId();

    void addToComments(Theme theme, String commentDescription, String userName);

    void deleteCommentById(Long id);
}
