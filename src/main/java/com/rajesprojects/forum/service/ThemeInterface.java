package com.rajesprojects.forum.service;

import com.rajesprojects.forum.entity.Theme;

import java.util.List;

public interface ThemeInterface {

    List<Theme> getThemes();

    Theme getThemeById(Long themeId);

    Long getId();

    void addToThemes(Long personId, String themeName, String themeDescription);

    void deleteThemeById(Long id);
}
