package com.rajesprojects.forum.repository;

import com.rajesprojects.forum.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface PeopleRepository extends JpaRepository<Person, Long> {
    Optional<Person> findByUsername(String username);

    Person findPersonByUsername(String username);
}
