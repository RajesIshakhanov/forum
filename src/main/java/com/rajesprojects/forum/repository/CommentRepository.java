package com.rajesprojects.forum.repository;

import com.rajesprojects.forum.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByThemeId(Long id);

    @Query(value = "SELECT last_value from comment_id_seq", nativeQuery = true)
    Long getCurrentVal();


    void deleteAllByThemeId(Long id);
}
