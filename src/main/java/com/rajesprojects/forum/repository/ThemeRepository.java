package com.rajesprojects.forum.repository;

import com.rajesprojects.forum.entity.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ThemeRepository extends JpaRepository<Theme, Long> {

    @Query(value = "SELECT last_value from theme_id_seq", nativeQuery = true)
    Long getCurrentVal();
}
