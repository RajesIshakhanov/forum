package com.rajesprojects.forum.util;

import com.rajesprojects.forum.entity.Person;
import com.rajesprojects.forum.service.PersonDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class PersonValidator implements Validator {

    private final PersonDetailsService personDetailsService;

    @Autowired
    public PersonValidator(PersonDetailsService personDetailsService) {
        this.personDetailsService = personDetailsService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Person.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Person person = (Person) target;

        if (person.getEmail().isEmpty() || person.getUsername().isEmpty() || person.getPassword().isEmpty()) {
            errors.reject("", "username, password and email should not be empty");
        }

        if (person.getPassword().length() < 5) {
            errors.rejectValue("password", "", "Password length should not be less than 5 symbols");
        }

        try {
            personDetailsService.loadUserByUsername(person.getUsername());
        } catch (UsernameNotFoundException ignored) {
            return;
        }

        errors.rejectValue("username", "", "A user with that username already exists.");


    }
}
