package com.rajesprojects.forum.controller;

import com.rajesprojects.forum.entity.Person;
import com.rajesprojects.forum.entity.Theme;
import com.rajesprojects.forum.service.CommentService;
import com.rajesprojects.forum.service.PersonDetailsService;
import com.rajesprojects.forum.service.ThemeService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping
@Validated
@AllArgsConstructor
public class ThemeController {
    private final ThemeService themesService;
    private final PersonDetailsService personDetailsService;
    private final CommentService commentService;

    @GetMapping("/forum")
    public String showForum(Model model) {
        List<Theme> themes = themesService.getThemes();
        model.addAttribute("themes", themes);
        return "forumguest";
    }

    @GetMapping("/theme")
    public String showThemes(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Long currentPrincipalId = personDetailsService.getPersonIdByName(currentPrincipalName);
        Person person = personDetailsService.getPersonByName(currentPrincipalName);
        List<Theme> themes = themesService.getThemes();
        model.addAttribute("themes", themes);
        model.addAttribute("username", currentPrincipalName);
        model.addAttribute("userid", currentPrincipalId);
        if (person.getRole().equals("ROLE_ADMIN")) {
            return "themes-admin";
        }
        return "themes";
    }

    @PostMapping("/theme/add")
    public String addNewThemeByUserId(@RequestParam Long userId) {
        return "redirect:/themes/add/" + userId;
    }

    @GetMapping("/themes/add/{userId}")
    public String createTheme(@PathVariable String userId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("userId", userId);
        model.addAttribute("username", currentPrincipalName);
        return "newtheme";
    }

    @PostMapping("/newtheme/add")
    public String addNewTheme(@RequestParam Long userId, @RequestParam String themeName,  @RequestParam String themeDescription) {
        themesService.addToThemes(userId, themeName, themeDescription);
        return "redirect:/theme";
    }

    @PostMapping("/theme-delete")
    public String deleteTheme(@RequestParam Long themeId, @RequestParam String userName) {
        themesService.deleteThemeById(themeId);
        return "redirect:/theme";
    }
}
