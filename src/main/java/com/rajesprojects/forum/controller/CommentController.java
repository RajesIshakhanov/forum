package com.rajesprojects.forum.controller;

import com.rajesprojects.forum.entity.Comment;
import com.rajesprojects.forum.entity.Person;
import com.rajesprojects.forum.entity.Theme;
import com.rajesprojects.forum.service.CommentService;
import com.rajesprojects.forum.service.PersonDetailsService;
import com.rajesprojects.forum.service.ThemeService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping
@Validated
@AllArgsConstructor
public class CommentController {
    private final CommentService commentsService;
    private final PersonDetailsService personDetailsService;
    private final ThemeService themeService;

    @PostMapping("/readcomment")
    public String readCommentsByThemeId(@RequestParam Long themeId) {
        return "redirect:/readcomment/" + themeId;
    }

    @GetMapping("/readcomment/{themeId}")
    public String uploadCommentsByThemeId(@PathVariable Long themeId, Model model) {
        Theme theme = themeService.getThemeById(themeId);
        String themeDescription = theme.getDescription();
        String themeName = theme.getName();
        List<Comment> comments = commentsService.getByThemeId(themeId);
        model.addAttribute("comments", comments);
        model.addAttribute("description", themeDescription);
        model.addAttribute("themeName", themeName);
        return "commentsguest";
    }

    @PostMapping("/comment")
    public String getCommentsByThemeId(@RequestParam Long themeId, @RequestParam Long userId) {
        return "redirect:/comment/" + themeId;
    }

    @GetMapping("/comment/{themeId}")
    public String showCommentsByThemeId(@PathVariable Long themeId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Person person = personDetailsService.getPersonByName(currentPrincipalName);
        List<Comment> comments = commentsService.getByThemeId(themeId);
        Theme theme = themeService.getThemeById(themeId);
        String themeDescription = theme.getDescription();
        String themeName = theme.getName();
        model.addAttribute("comments", comments);
        model.addAttribute("username", currentPrincipalName);
        model.addAttribute("themeid", themeId);
        model.addAttribute("description", themeDescription);
        model.addAttribute("themeName", themeName);
        if (person.getRole().equals("ROLE_ADMIN")) {
            return "comments-admin";
        }
        return "comments";
    }

    @PostMapping("/comment/add")
    public String addNewComment(@RequestParam Long themeId) {
        return "redirect:/comment/add/" + themeId;
    }

    @GetMapping("/comment/add/{themeId}")
    public String createComment(@PathVariable String themeId, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        model.addAttribute("themeId", themeId);
        model.addAttribute("username", currentPrincipalName);
        return "newcomment";
    }

    @PostMapping("/newcomment/add")
    public String addNewComment(@RequestParam Long themeId, @RequestParam String commentDescription) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        Theme theme = themeService.getThemeById(themeId);
        commentsService.addToComments(theme, commentDescription, currentPrincipalName);
        return "redirect:/theme";
    }

    @PostMapping("/comment-delete")
    public String deleteComment(@RequestParam Long commentId, @RequestParam Long themeId, @RequestParam String userName) {
        commentsService.deleteCommentById(commentId);
        return "redirect:/comment/" + themeId;
    }

}
