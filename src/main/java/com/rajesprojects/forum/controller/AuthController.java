package com.rajesprojects.forum.controller;

import com.rajesprojects.forum.entity.Person;
import com.rajesprojects.forum.service.RegistrationService;
import com.rajesprojects.forum.util.PersonValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/auth")
public class AuthController {

    private final RegistrationService registrationService;
    private final PersonValidator personValidator;

    @Autowired
    public AuthController(RegistrationService registrationService, PersonValidator personValidator) {
        this.registrationService = registrationService;
        this.personValidator = personValidator;
    }

    @GetMapping("/login")
    public String loginPage() {
        return "auth/login";
    }

    @GetMapping("/login-error")
    public String loginErrorPage() {
        return "auth/login-error";
    }



    @GetMapping("/registration")
    public String registrationPage(@ModelAttribute("person") Person person, Model model) {
        return "auth/registration";
    }

    @PostMapping("/registration")
    public String performRegistration(@ModelAttribute("person") @Valid Person person, Model model, BindingResult bindingResult) {

        personValidator.validate(person, bindingResult);

        if (bindingResult.hasErrors()) {
            return "auth/registration-error";
        }

        registrationService.register(person);

        return "redirect:/auth/login";
    }

}
