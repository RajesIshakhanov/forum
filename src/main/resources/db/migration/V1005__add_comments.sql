insert into comment(theme_id, person_id, body, created_date)
values (1, 1, 'Theme 1, comments body 1', '2022-11-24'),
       (1, 1, 'Theme 1, comments body 2', '2022-11-24'),
       (2, 1, 'Theme 2, comments body 1', '2022-11-24'),
       (2, 1, 'Theme 2, comments body 2', '2022-11-24'),
       (3, 1, 'Theme 3, comments body 1', '2022-11-24'),
       (3, 1, 'Theme 3, comments body 2', '2022-11-24');