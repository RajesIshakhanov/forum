create table theme (
    id bigserial PRIMARY KEY,
    person_id bigserial,
    name varchar(100) not null,
    description varchar(100) not null,
    created_date date,
    status varchar(50) not null,
    constraint fk_person
       foreign key (person_id)
           references person (id)
);

create table tag (
    id bigserial PRIMARY KEY,
    name varchar(100) not null
);

create table theme_tag (
    theme_id bigserial,
    tag_id bigserial,
    primary key (theme_id, tag_id),
    constraint fk_theme
       foreign key (theme_id)
           references theme (id),
    constraint fk_tag
       foreign key (tag_id)
           references tag (id)
);