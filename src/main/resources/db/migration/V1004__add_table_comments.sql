create table comment (
    id bigserial PRIMARY KEY,
    theme_id bigserial,
    person_id bigserial,
    body varchar(2000) not null,
    created_date date,
    constraint fk_theme
     foreign key (theme_id)
         references theme (id),
    constraint fk_person
     foreign key (person_id)
         references person (id)
);