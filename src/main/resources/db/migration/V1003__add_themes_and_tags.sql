insert INTO theme(person_id, name, description, created_date, status)
VALUES (1, 'Theme name 1', 'Theme 1 description 1', '2022-11-29', 'open'),
       (1, 'Theme name 2', 'Theme 2 description 1', '2022-11-29', 'open'),
       (1, 'Theme name 3', 'Theme 3 description 1', '2022-11-29', 'open');

insert into tag(name)
values ('tag1'),
       ('tag2');

insert into theme_tag (theme_id, tag_id) VALUES (1, 1);
insert into theme_tag (theme_id, tag_id) VALUES (2, 1);
insert into theme_tag (theme_id, tag_id) VALUES (3, 2);