create table person(
    id bigserial PRIMARY KEY,
    username varchar(100) not null,
    password varchar(100) not null,
    email varchar(100) not null,
    role varchar(100) not null,
    enabled boolean not null
);

insert into person(username, password, email, role, enabled)
values ('admin', '$2a$12$A4QwfujaATpAw/sftaDJJuQQGkcPmSW/xm7L1EiAJ7OKw/mB1ajPK', 'admin@gmail.com', 'ROLE_ADMIN', true);

insert into person(username, password, email, role, enabled)
values ('user1', '$2a$12$NR1ibOoH/Msafwle3waMJeP3RBgUB1bG8TbgJgySuLe6eVjawoL/q', 'user1@gmail.com', 'ROLE_USER', true);